import 'package:flutter/material.dart';
import 'package:quotewear/cart.dart';
import 'package:quotewear/cart3.dart';


class Item3 extends StatefulWidget {
  const Item3({super.key});

  @override
  State<Item3> createState() => _Item3();
}

class _Item3 extends State<Item3> {
  @override
  Widget build(BuildContext context) {
    //end
    return Scaffold(
      appBar: AppBar(
        title: const Text('QuoteWear'),
        backgroundColor: const Color(0xffFF0000),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.shopping_bag_outlined),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Cart3()),
              );
            },
          )
        ],
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(5),
                bottomRight: Radius.circular(5))),
      ),
      body: SafeArea(
        child: Column(mainAxisAlignment: MainAxisAlignment.start, children: [
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(18, 0, 18, 0),
            child: Image.asset(
              'assets/image/clothing3.png',
              fit: BoxFit.fill,
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'One Goal Shirt',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 24,
              fontFamily: 'Roboto',
            ),
          ),
          const Text(
            '₱320',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 24,
              fontFamily: 'Roboto',
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Product Description',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 20,
              fontFamily: 'Roboto',
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Column(
            children: const [
              Text(
                'Cotton',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 18,
                  fontFamily: 'Roboto',
                ),
              ),
              Text(
                'Asian Size',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 18,
                  fontFamily: 'Roboto',
                ),
              ),
              Text(
                'Premium Quality',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 18,
                  fontFamily: 'Roboto',
                ),
              ),
              Text(
                'Soft, Stretchable, and Comfortable',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 18,
                  fontFamily: 'Roboto',
                ),
              ),
              Text(
                'Printed Design',
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: 18,
                  fontFamily: 'Roboto',
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          const Text(
            'Size',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 20,
              fontFamily: 'Roboto',
            ),
          ),
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(0, 15, 0, 0),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(7, 0, 7, 0),
                  child: OutlinedButton(
                    onPressed: () {},
                    style: OutlinedButton.styleFrom(),
                    child: const Text(
                      'S',
                      style: TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0)), //Custom Test
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(7, 0, 7, 0),
                  child: OutlinedButton(
                    onPressed: () {},
                    style: OutlinedButton.styleFrom(),
                    child: const Text(
                      'M',
                      style: TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0)), //Custom Test
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(7, 0, 7, 0),
                  child: OutlinedButton(
                    onPressed: () {},
                    style: OutlinedButton.styleFrom(),
                    child: const Text(
                      'L',
                      style: TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0)), //Custom Test
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsetsDirectional.fromSTEB(7, 0, 7, 0),
                  child: OutlinedButton(
                    onPressed: () {},
                    style: OutlinedButton.styleFrom(),
                    child: const Text(
                      'XL',
                      style: TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0)), //Custom Test
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]),

        //---------------------------------------------------------//
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => const Cart3()));
        },
        tooltip: 'Increment',
        backgroundColor: const Color(0xffFF0000),
        label: const Text('Add to cart'),
        elevation: 10,
        extendedPadding: const EdgeInsets.all(100.0),
      ),
    );
  }
}
