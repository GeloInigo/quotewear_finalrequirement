import 'package:flutter/material.dart';
import 'package:quotewear/order3.dart';

void main() {
  runApp(
    const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Sample4(),
    ),
  );
}

class Sample4 extends StatefulWidget {
  const Sample4({Key? key}) : super(key: key);

  @override
  _Sample4State createState() => _Sample4State();
}

class _Sample4State extends State<Sample4> {
  TextEditingController provinceController = TextEditingController();
  TextEditingController municipalityController = TextEditingController();
  TextEditingController zipcodeController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  String selectedOption = 'Payment Method';

  @override
  void dispose() {
    provinceController.dispose();
    municipalityController.dispose();
    zipcodeController.dispose();
    streetController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Shopping Bag'),
        backgroundColor: const Color(0xffFF0000),
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(5),
            bottomRight: Radius.circular(5),
          ),
        ),
      ),
      body: Center(
        child: Container(
          child: ElevatedButton(
            onPressed: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('Enter your location...'),
                    contentPadding: EdgeInsets.all(20),
                    content: SizedBox(
                      height: 344,
                      width: 550, // Adjust the height as desired
                      child: buildDropdownSelection(),
                    ),
                    actions: [
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: const Color(0xffFF0000),
                          ),
                          onPressed: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const Order3(),
                              ),
                            );
                          },
                          child: const Text("Submit"),
                        ),
                      ),
                    ],
                  );
                },
              );
            },
            child: const Text('Open Dialog'),
          ),
        ),
      ),
    );
  }

  Widget buildDropdownSelection() {
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: provinceController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Province',
              ),
            ),
            const SizedBox(height: 15),
            TextField(
              controller: municipalityController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Municipality',
              ),
            ),
            const SizedBox(height: 15),
            TextField(
              controller: zipcodeController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Zipcode',
              ),
            ),
            const SizedBox(height: 15),
            TextField(
              controller: streetController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Street/Barangay',
              ),
            ),
            const SizedBox(height: 15),
            Column(
              children: [
                Center(
                  child: DropdownButton<String>(
                    value: selectedOption,
                    items: <String>[
                      'Payment Method',
                      'Cash on delivery',
                      'Gcash',
                      'Paymaya',
                    ].map<DropdownMenuItem<String>>(
                      (String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(
                            value,
                            style: TextStyle(
                              color: value == 'Payment Method'
                                  ? const Color.fromARGB(255, 176, 176,
                                      176) // Make the item gray if it's "Payment Method"
                                  : Colors
                                      .black, // Set the regular text color for other items
                            ),
                          ),
                          onTap: () {
                            if (value == 'Payment Method') {
                              // Disable selection if it's "Payment Method"
                            }
                          },
                        );
                      },
                    ).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        selectedOption = newValue!;
                      });
                    },
                    style: TextStyle(
                      color: Colors.white,
                    ),
                    dropdownColor: Colors.white,
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
