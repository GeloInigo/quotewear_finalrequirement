import 'package:flutter/material.dart';

void main() {
  runApp(
    const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Ordered(),
    ),
  );
}

class Ordered extends StatefulWidget {
  const Ordered({Key? key}) : super(key: key);

  @override
  _OrderedState createState() => _OrderedState();
}

class _OrderedState extends State<Ordered> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your order'),
        backgroundColor: const Color(0xffFF0000),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(5),
                bottomRight: Radius.circular(5))),
      ),
      body: Container(
        color: const Color(0xFFEEEFF5),
        padding: const EdgeInsets.only(top: 16, bottom: 1),
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(10, 0, 10, 20),
          child: Column(
            children: [
              Stack(
                children: [
                  SizedBox(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      padding: const EdgeInsets.only(
                        left: 330,
                        top: 0,
                        right: 200,
                        bottom: 90,
                      ),
                      width: double.infinity,
                      height: 160,
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 217, 217, 217),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20, left: 0),
                    child: Image.asset(
                      'assets/image/clothing1.png',
                      width: 140,
                      height: 130,
                    ),
                  ),
                  Row(
                    children: [
                      Row(
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(left: 120, top: 45),
                            child: Text(
                              'Inspire Shirt',
                              style: TextStyle(
                                fontSize: 20,
                                fontFamily: 'Jua',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Row(
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(left: 120, top: 70),
                            child: Text(
                              'Size: L',
                              style: TextStyle(
                                fontSize: 13,
                                fontFamily: 'Jua',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Row(
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(left: 120, top: 85),
                            child: Text(
                              'Qty.2',
                              style: TextStyle(
                                fontSize: 13,
                                fontFamily: 'Jua',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Row(
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(left: 120, top: 100),
                            child: Text(
                              'Price: ₱600',
                              style: TextStyle(
                                fontSize: 13,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Row(
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(left: 120, top: 115),
                            child: Text(
                              'Your parcel is out for delivery...',
                              style: TextStyle(
                                fontSize: 13,
                                fontFamily: 'Roboto',
                                fontWeight: FontWeight.bold,
                                color: Color(0xFFFF0000),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
