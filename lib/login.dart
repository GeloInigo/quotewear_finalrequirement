import 'package:flutter/material.dart';
import 'package:quotewear/home.dart';
import 'package:quotewear/register.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool _securePassword = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(
            height: 40,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Align(
                alignment: AlignmentDirectional(0, 0),
                child: Text(
                  'Quote',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 38),
                ),
              ),
              Text(
                'Wear',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 38,
                    color: Color(0xFFFF0000)),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(18, 70, 18, 0),
            child: TextField(
              decoration: InputDecoration(
                hintText: 'Email',
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                    color: Color(0xff9F9797),
                    width: 1.0,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                    color: Color(0xff000000),
                    width: 1.5,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(18, 35, 18, 0),
            child: TextField(
              obscureText: _securePassword,
              decoration: InputDecoration(
                hintText: 'Password',
                suffixIcon: IconButton(
                  onPressed: () {
                    setState(() {
                      _securePassword = !_securePassword;
                    });
                  },
                  icon: Icon(_securePassword
                      ? Icons.visibility
                      : Icons.visibility_off),
                ),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                    color: Color(0xff9F9797),
                    width: 1.0,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(10),
                  borderSide: const BorderSide(
                    color: Color(0xff000000),
                    width: 1.5,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(0, 60, 0, 0),
            child: ElevatedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Home()));
              },
              style: ElevatedButton.styleFrom(
                elevation: 0,
                backgroundColor: const Color(0xffFF0000),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              child: const Padding(
                padding: EdgeInsetsDirectional.fromSTEB(30, 15, 30, 15),
                child: Text(
                  'Login',
                  style: TextStyle(fontSize: 16), //Custom Test
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsetsDirectional.fromSTEB(0, 20, 0, 0),
            child: OutlinedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Register()));
              },
              style: OutlinedButton.styleFrom(),
              child: const Padding(
                padding: EdgeInsetsDirectional.fromSTEB(3, 13, 3, 13),
                child: Text(
                  'Create Account',
                  style: TextStyle(color: Color(0xff9F9797)), //Custom Test
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Image.asset(
            'assets/image/or.png',
            width: 150,
            height: 60,
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                'assets/image/ggoogle.png',
                width: 60,
                height: 60,
              ),
              const SizedBox(
                width: 20,
              ),
              Image.asset(
                'assets/image/aapple.png',
                width: 60,
                height: 60,
              ),
            ],
          )
        ],
      ),
    );
  }
}
