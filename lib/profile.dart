import 'package:flutter/material.dart';
import 'package:quotewear/login.dart';
import 'package:quotewear/order.dart';
import 'package:quotewear/ordered.dart';

class Profile extends StatefulWidget {
  const Profile({super.key});

  @override
  State<Profile> createState() => _Profile();
}

class _Profile extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    //end
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
        backgroundColor: const Color(0xffFF0000),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.delivery_dining),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Ordered()),
              );
            },
          )
        ],
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(5),
                bottomRight: Radius.circular(5))),
      ),
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            const SizedBox(height: 25),
            Align(
              alignment: const AlignmentDirectional(0, -0.85),
              child: Container(
                width: 150,
                height: 150,
                clipBehavior: Clip.antiAlias,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  'assets/image/my_image.png',
                  fit: BoxFit.fill,
                ),
              ),
            ),
            const SizedBox(height: 30),
            SizedBox(
                width: 341,
                height: 250,
                child: Stack(children: <Widget>[
                  Positioned(
                      top: 0,
                      left: 0,
                      child: Container(
                          width: 341,
                          height: 170,
                          decoration: BoxDecoration(
                            boxShadow: const [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.25),
                                  offset: Offset(0, 4),
                                  blurRadius: 4)
                            ],
                            color: const Color.fromRGBO(255, 255, 255, 1),
                            border: Border.all(
                              color: const Color.fromRGBO(255, 255, 255, 1),
                              width: 1,
                            ),
                          ))),
                  const Positioned(
                      top: 56,
                      left: 15,
                      child: Divider(
                          color: Color.fromRGBO(43, 42, 42, 1), thickness: 1)),
                  const Positioned(
                      top: 25, left: 25, child: Icon(Icons.person)),
                  const Positioned(
                      top: 28,
                      left: 65,
                      child: Text(
                        'Christian Angelo Doria Inigo',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 0.5),
                            fontFamily: 'Inter',
                            fontSize: 15,
                            letterSpacing:
                                0 /*percentages not used in flutter. defaulting to zero*/,
                            fontWeight: FontWeight.normal,
                            height: 1),
                      )),
                  const Positioned(top: 71, left: 25, child: Icon(Icons.email)),
                  const Positioned(
                      top: 76,
                      left: 65,
                      child: Text(
                        'angelo@gmail.com',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 0.5),
                            fontFamily: 'Inter',
                            fontSize: 15,
                            letterSpacing:
                                0 /*percentages not used in flutter. defaulting to zero*/,
                            fontWeight: FontWeight.normal,
                            height: 1),
                      )),
                  const Positioned(top: 120, left: 25, child: Icon(Icons.call)),
                  const Positioned(
                      top: 124,
                      left: 65,
                      child: Text(
                        '09612152665',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                            color: Color.fromRGBO(0, 0, 0, 0.5),
                            fontFamily: 'Inter',
                            fontSize: 15,
                            letterSpacing:
                                0 /*percentages not used in flutter. defaulting to zero*/,
                            fontWeight: FontWeight.normal,
                            height: 1),
                      )),
                ])),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => const Login()));
        },
        tooltip: 'Increment',
        backgroundColor: const Color(0xffFF0000),
        label: const Text('Logout'),
        elevation: 10,
        extendedPadding: const EdgeInsets.all(100.0),
      ),
    );
  }
}
