import 'package:flutter/material.dart';

class Buttonwidget extends StatefulWidget {
  const Buttonwidget({super.key});

  @override
  State<Buttonwidget> createState() => _ButtonwidgetState();
}

class _ButtonwidgetState extends State<Buttonwidget> {
  @override
  Widget build(BuildContext context) {
    //end
    return Scaffold(
      appBar: AppBar(
        title: const Text('Button Widget'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            //---------------------------------------------------------//
            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                  elevation: 0,
                  backgroundColor: Colors.blue,
                  shape: const StadiumBorder(),
                  textStyle: const TextStyle(fontSize: 20)),
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  'StadiumBorder',
                  style: TextStyle(fontSize: 18.0),
                ),
              ),
            ),
            //---------------------------------------------------------//
            const SizedBox(
              height: 10,
            ),
            //---------------------------------------------------------//
            ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                elevation: 10,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15), // <-- Radius
                ),
              ),
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text('Custom Button'),
              ),
            ),
            //---------------------------------------------------------//
            //Outlined Button
            OutlinedButton(
              onPressed: () {},
              style: OutlinedButton.styleFrom(
                side: const BorderSide(width: 1.0, color: Colors.blue),
                shape: const StadiumBorder(),
              ),
              child: const Text(
                'Outlined button',
                style: TextStyle(color: Colors.black), //Custom Test
              ),
            ),
            //---------------------------------------------------------//
            OutlinedButton(
              onPressed: () {},
              style: OutlinedButton.styleFrom(
                side: const BorderSide(width: 1.0, color: Colors.red),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
              ),
              child: const Text('Custom outlined'),
            )
          ],
        ),
      ),
    );
  }
}
