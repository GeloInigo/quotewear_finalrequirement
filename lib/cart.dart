import 'package:flutter/material.dart';
import 'package:quotewear/order.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:quotewear/order3.dart';

void main() {
  runApp(
    const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Cart(),
    ),
  );
}

class Cart extends StatefulWidget {
  const Cart({Key? key}) : super(key: key);

  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  TextEditingController provinceController = TextEditingController();
  TextEditingController municipalityController = TextEditingController();
  TextEditingController zipcodeController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  String selectedOption = 'Payment Method';
  int quantity = 1;

  void decreaseQuantity() {
    setState(() {
      if (quantity > 1) {
        quantity--;
      }
    });
  }

  void increaseQuantity() {
    setState(() {
      quantity++;
    });
  }

  @override
  void dispose() {
    provinceController.dispose();
    municipalityController.dispose();
    zipcodeController.dispose();
    streetController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Shopping Bag'),
        backgroundColor: const Color(0xffFF0000),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(5),
                bottomRight: Radius.circular(5))),
      ),
      body: Container(
        padding: const EdgeInsets.only(top: 16, bottom: 1),
        child: Padding(
          padding: const EdgeInsetsDirectional.fromSTEB(10, 0, 10, 20),
          child: Column(
            children: [
              Stack(
                children: [
                  SizedBox(
                    child: Container(
                      margin: const EdgeInsets.only(top: 10),
                      padding: const EdgeInsets.only(
                        left: 330,
                        top: 0,
                        right: 200,
                        bottom: 90,
                      ),
                      width: double.infinity,
                      height: 120,
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 226, 226, 226),
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 30, left: 0),
                    child: Image.asset(
                      'assets/image/clothing1.png',
                      width: 110,
                      height: 80,
                    ),
                  ),
                  Row(
                    children: [
                      Row(
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(left: 95, top: 42),
                            child: Text(
                              'Inspire Shirt',
                              style: TextStyle(
                                fontSize: 17,
                                fontFamily: 'Jua',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Row(
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(left: 95, top: 63),
                            child: Text(
                              'Size: L',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'Jua',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Row(
                        children: const [
                          Padding(
                            padding: EdgeInsets.only(left: 95, top: 82),
                            child: Text(
                              '₱300',
                              style: TextStyle(
                                fontSize: 15,
                                fontFamily: 'Jua',
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  Positioned(
                    right: 10,
                    bottom: 47,
                    width: 106,
                    height: 25,
                    child: Container(
                      padding: const EdgeInsets.only(right: 1),
                      decoration: const BoxDecoration(
                        color: Colors.white,
                      ),
                      child: Row(
                        children: [
                          IconButton(
                            icon: const Icon(
                              Icons.remove,
                              color: Colors.black,
                              size: 12,
                            ),
                            onPressed: decreaseQuantity,
                          ),
                          Container(
                            padding: const EdgeInsets.only(right: 2),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(3),
                              color: Colors.white,
                            ),
                            child: Text(
                              quantity.toString(),
                              style: const TextStyle(
                                color: Colors.black,
                                fontSize: 12,
                              ),
                            ),
                          ),
                          IconButton(
                            icon: const Icon(
                              Icons.add,
                              color: Colors.black,
                              size: 12,
                            ),
                            onPressed: increaseQuantity,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: const Text('Enter your location...'),
                contentPadding: EdgeInsets.all(20),
                content: SizedBox(
                  height: 344,
                  width: 550, // Adjust the height as desired
                  child: buildDropdownSelection(),
                ),
                actions: [
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0xffFF0000),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const Order(),
                          ),
                        );
                      },
                      child: const Text("Submit"),
                    ),
                  ),
                ],
              );
            },
          );
        },
        tooltip: 'Increment',
        backgroundColor: const Color(0xffFF0000),
        label: const Text('Check Out'),
        elevation: 10,
        extendedPadding: const EdgeInsets.all(100.0),
      ),
    );
  }

  Widget buildDropdownSelection() {
    return StatefulBuilder(
      builder: (BuildContext context, StateSetter setState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: provinceController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Province',
              ),
            ),
            const SizedBox(height: 15),
            TextField(
              controller: municipalityController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Municipality',
              ),
            ),
            const SizedBox(height: 15),
            TextField(
              controller: zipcodeController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Zipcode',
              ),
            ),
            const SizedBox(height: 15),
            TextField(
              controller: streetController,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
                hintText: 'Street/Barangay',
              ),
            ),
            const SizedBox(height: 15),
            Column(
              children: [
                Center(
                  child: DropdownButton<String>(
                    value: selectedOption,
                    items: <String>[
                      'Payment Method',
                      'Cash on delivery',
                      'GCash',
                      'PayMaya',
                    ].map<DropdownMenuItem<String>>(
                      (String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(
                            value,
                            style: TextStyle(
                              color: value == 'Payment Method'
                                  ? const Color.fromARGB(255, 176, 176,
                                      176) // Make the item gray if it's "Payment Method"
                                  : Colors
                                      .black, // Set the regular text color for other items
                            ),
                          ),
                          onTap: () {
                            if (value == 'Payment Method') {
                              // Disable selection if it's "Payment Method"
                            }
                          },
                        );
                      },
                    ).toList(),
                    onChanged: (String? newValue) {
                      setState(() {
                        selectedOption = newValue!;
                      });
                    },
                    style: const TextStyle(
                      color: Colors.white,
                    ),
                    dropdownColor: Colors.white,
                  ),
                ),
              ],
            ),
          ],
        );
      },
    );
  }
}
