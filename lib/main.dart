import 'package:flutter/material.dart';
import 'package:quotewear/loadingscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "LS",
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: const Loadingscreen(),
    );
  }
}
